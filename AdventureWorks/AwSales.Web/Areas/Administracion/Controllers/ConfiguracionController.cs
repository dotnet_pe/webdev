﻿using AwSales.Funcionalidades.Configuracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AwSales.Web.Areas.Administracion.Controllers
{
    
    public class ConfiguracionController : Controller
    {
        // GET: Administracion/Configuracion
        public ActionResult Ver()
        {
            return View(new VerConfiguracionModelo());
        }

        public ActionResult IrAHome()
        {
            //quiero ir al home de la raiz
            return RedirectToAction("Contact", "Home", new { area = "" });
        }
    }
}