﻿using AwSales.Web.Funcionalidades;
using AwSales.Web.Funcionalidades.EditarEmpleado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AwSales.Web.Controllers
{
    
    public class EmpleadosController : Controller
    {
        // GET: Empleados
        public ActionResult Lista()
        {

            using (var listarEmpleados = new ListarEmpleados())
            {
                return View(listarEmpleados.Ejecutar());
            }
        }

        [HttpGet]
        public ActionResult CambiarCargo(int id)
        {
            using (var comando = new TraeEmpleadoParaCambioDeCargo())
            {
                return View(comando.Ejecutar(id));
            }
        }


        [HttpPost]
        public ActionResult CambiarCargo(CambiarCargoAEmpleadoComando modelo)
        {
            using (var comando = new CambiarCargoAEmpleado())
            {
                try
                {
                    comando.Ejecutar(modelo);
                    return RedirectToAction("Lista");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Hubo un error al grabar");
                    using (var comando2 = new TraeEmpleadoParaCambioDeCargo())
                    {
                        return View(comando2.Ejecutar(modelo.EmpleadoId));
                    }
                }
            }
        }
    }
}