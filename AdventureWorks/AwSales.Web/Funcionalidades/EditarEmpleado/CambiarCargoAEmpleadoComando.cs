﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwSales.Web.Funcionalidades.EditarEmpleado
{
    public class CambiarCargoAEmpleadoComando
    {
        public int EmpleadoId { get; set; }
        public int CargoSeleccionado { get; set; }
    }
}
