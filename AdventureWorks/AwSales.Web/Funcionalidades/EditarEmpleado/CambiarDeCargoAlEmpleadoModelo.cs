﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AwSales.Web.Funcionalidades.EditarEmpleado
{
    public class CambiarDeCargoAlEmpleadoModelo
    {
        private readonly IEnumerable<CargoDisponible> _cargos;

        public int EmpleadoId { get; set; }
        public string Nombre { get; set; }

        public int CargoSeleccionado { get; set; }

        public SelectList Cargos
        {
            get
            {
                return new SelectList(_cargos, "CargoId", "Descripcion", CargoSeleccionado);
            }
        }

        public CambiarDeCargoAlEmpleadoModelo(IEnumerable<CargoDisponible> cargos)
        {
            _cargos = cargos;
        }
    }

    public class CargoDisponible
    {
        public int CargoId { get; set; }
        public string Descripcion { get; set; }
    }
}
