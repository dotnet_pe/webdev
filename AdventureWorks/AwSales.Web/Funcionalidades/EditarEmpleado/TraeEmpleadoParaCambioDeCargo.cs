﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwSales.Web.Funcionalidades.EditarEmpleado
{
    public class TraeEmpleadoParaCambioDeCargo : IDisposable
    {
        public void Dispose()
        {
            // cerrar todos los recursos
        }

        public CambiarDeCargoAlEmpleadoModelo Ejecutar(int id)
        {
            // consultar a la BD de Empleado por id
            // traer la lista de cargos
            var listaCargos = new List<CargoDisponible>()
            {
                new CargoDisponible {CargoId=1,Descripcion ="Gerente"},
                new CargoDisponible {CargoId=2,Descripcion ="Sectorista"},
                new CargoDisponible {CargoId=3,Descripcion ="Programador"}
            };
            var modelo = new CambiarDeCargoAlEmpleadoModelo(listaCargos)
            {
                EmpleadoId = id,  // traerlo del BD
                Nombre = "Uzi Mamani",
                CargoSeleccionado = 3
            };

            return modelo;
        }


    }
}
