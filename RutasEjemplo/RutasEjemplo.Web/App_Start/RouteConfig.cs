﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RutasEjemplo.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
            name: "BandejaUsuario",
            url: "{usuario}/{action}",
            defaults: new { controller = "Perfil" },
            constraints: new { action = "bandeja|movimientos|perfil|historial" }
            );

            routes.MapRoute(
                name: "reportes",
                url: "{sede}/reportes/{controller}/{anio}/{mes}/{dia}",
                defaults: new
                {
                    controller = "articulos",
                    action = "reporte",
                    anio = UrlParameter.Optional, // DateTime.Now.Year,
                    mes = UrlParameter.Optional, //DateTime.Now.Month,
                    dia = UrlParameter.Optional //DateTime.Now.Day
                },
                constraints: new { anio = @"\d{4}", mes = @"\d{0,2}", dia = @"\d{0,2}" }
                );


            routes.MapRoute(
                name: "ArticulosxId",
                url: "articulos/{id}",
                defaults: new { controller = "Articulos", action = "Detalle" },
                constraints: new { id = @"\d+" }
                );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
