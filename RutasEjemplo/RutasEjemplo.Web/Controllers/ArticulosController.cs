﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RutasEjemplo.Web.Controllers
{
    public class ArticulosController : Controller
    {
        // GET: Articulos
        public ActionResult Detalle(int id)
        {
            ViewBag.ArticuloId = id;
            return View();
        }
        [Route("{sede}/reportes/articulos/{anio:int?}/{mes:int?}/{dia:int?}")]
        public ActionResult Reporte(string sede, int? anio, int? mes, int? dia)
        {
            // tiene que haber logica que resuelva la fecha
            ViewBag.Sede = sede;
            ViewBag.Anio = anio.HasValue ? anio.Value.ToString() : "";
            ViewBag.Mes = mes.HasValue ? mes.Value.ToString() : "";
            ViewBag.Dia = dia.HasValue ? dia.Value.ToString() : "";

            return View();
        }
    }
}