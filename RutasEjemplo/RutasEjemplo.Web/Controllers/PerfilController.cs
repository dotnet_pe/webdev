﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RutasEjemplo.Web.Controllers
{
    public class PerfilController : Controller
    {
        // GET: Perfil
        public ActionResult Perfil(string usuario)
        {
            ViewBag.Usuario = usuario;
            return View();
        }

        public ActionResult Bandeja(string usuario)
        {
            ViewBag.Usuario = usuario;
            return View();

        }
    }
}