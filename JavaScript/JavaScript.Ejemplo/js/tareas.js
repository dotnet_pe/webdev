﻿var vista = (function () {
    var plantilla = '<li class="task"><input class="complete" type="checkbox" /> <input class="description" type="text" placeholder="Nombre/Descripcion de la Tarea..." /> <button class="delete-button">Borrar</button></li>';
    function mapeaTarea(tarea) {
        var $tarea = $(plantilla);
        if (tarea.terminada) {
            $tarea.find('.complete').attr('checked', 'checked');
        }
        $tarea.find('.description').val(tarea.descripcion);
        return $tarea;
    }

    function eliminarTarea(elemento) {
        $(elemento).closest(".task").remove();
    }

    function obtieneTarea(tareaHtml) {
        return {
            terminada: tareaHtml.find('.complete').prop('checked'),
            descripcion: tareaHtml.find('.description').val()
        };

    }
    function traerTareasDe(lista) {
        var tareas = [];
        $(lista).each(function (indice, tarea) {
            var nuevaTarea = obtieneTarea($(tarea));
            tareas.push(nuevaTarea);
        });
        return tareas

    }
    function traerTareas() {
        var pendientes = traerTareasDe('#task-list .task');
        var terminadas = traerTareasDe('#log-list .task');

        return pendientes.concat(terminadas);
    }

    function agregarTarea() {
        var listaTareas = $('#task-list');
        listaTareas.prepend(plantilla);
    }

    function agregarTareas(lista, tareas) {
        $(lista)
        .empty()
        .append(tareas);
    }
    function mostrarTareas(tareas) {
        var pendientes = [];
        var terminadas = [];
        for (var i = 0; i < tareas.length; i++) {
            var elemento = mapeaTarea(tareas[i]);
            if (tareas[i].terminada)
                terminadas.push(elemento);
            else
                pendientes.push(elemento);
        }
        agregarTareas('#task-list', pendientes);
        agregarTareas('#log-list', terminadas);
    }

    function completar(elemento) {
        var tarea = $(elemento).closest(".task")
        var tareaTerminada = obtieneTarea(tarea); // objeto json
        if (tareaTerminada.terminada) {
            $('#log-list').append(mapeaTarea(tareaTerminada)); // un nuevo elemento tarea
            tarea.remove();
        }
    }
    return {
        mostrar: mostrarTareas,
        agregar: agregarTarea,
        traerTareas: traerTareas,
        eliminar: eliminarTarea,
        completar: completar
    }

})();

var repositorio = (function () {
    var clave = "tareaJS";
    function leerTareas() {
        var tareas = localStorage.getItem(clave);
        if (tareas) {
            return JSON.parse(tareas);
        }
        return [];
    };

    function grabarTareas(tareas) {
        localStorage.setItem(clave, JSON.stringify(tareas));
    };
    function eliminar() {
        localStorage.removeItem(clave);
    }

    return {
        leer: leerTareas,
        grabar: grabarTareas,
        eliminar: eliminar
    }

})();

var controlador = (function () {
    function cancelar() {
        mostrarTareas();
    }

    function eliminar(event) {
        vista.eliminar(event.target);
    }

    function eliminarTodo() {
        repositorio.eliminar();
        mostrarTareas();
    }

    function grabarTarea() {
        var tareas = vista.traerTareas();
        repositorio.grabar(tareas);
    }

    function nuevaTarea() {
        vista.agregar();
    }

    function mostrarTareas() {
        var tareas = repositorio.leer();
        vista.mostrar(tareas);
    }

    function completar(event) {
        vista.completar(event.target);
    }

    return {
        nuevo: nuevaTarea,
        grabar: grabarTarea,
        eliminarTodo: eliminarTodo,
        eliminar: eliminar,
        cancelar: cancelar,
        mostrar: mostrarTareas,
        completar: completar
    }
})();

var app = (function () {
    function registrarEventos() {
        $('#new-task-button').on('click', controlador.nuevo);
        $('#save-button').on('click', controlador.grabar);
        $('#delete-all-button').on('click', controlador.eliminarTodo);
        $('#task-list').on('click', '.delete-button', controlador.eliminar);
        $('#task-list').on('change', '.complete', controlador.completar);
        $('#cancel-button').on('click', controlador.cancelar);
    };

    //publicar la interface de modulo
    return {
        iniciar: function () {
            registrarEventos();
            controlador.mostrar();
        }
    };
})();

$(function () {
    app.iniciar();
});


